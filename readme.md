
# Projet Portfolio
## Maquette Figma
J'ai commencé par dessiner une "maquette sur une feuille de papier.
Puis je suis passer sur Figma.\
<img src="public/img/../../../img/figmaPortefolio.png.png" width="100" height="100" />

## Structure HTML
J'ai réaliser ma structure html.\
Puis j'ai intégré Bootstrap.\
Avec les connaissances acquise en formation le matin j'ai pu améliorer la structure et le design.\
J'ai lié mon projet avec mon depot gitlab:
https://gitlab.com/sauvefanny/portfolio


Pour ce faire j'ai créé une page avec le nom
>.gitlab-ci.yml\
```
  - mkdir .public
  - cp -r * .public
  - mv .public public
 artifacts:
    paths:
     - public
 only:
  - master 
  ```
ce fichier à été 'push' sur mon dépôt ce qui a permis que mes 'commit' ce mettent à jour automatiquement sur ma page HTML en ligne avec comme lien:
https://sauvefanny.gitlab.io/portfolio

## Responsivité et Accessibilité

* J'ai tester le site afin qu'il soit responsive et valide W3C.

* J'ai testé l'accessibilité du site avec une extension comme WAVE.
